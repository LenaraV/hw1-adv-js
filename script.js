class Employee {
    constructor(name, age, salary){
        this._name = name;
        this._age = age;
        this.salary = salary;

    }
    get name(){
        return this.name;
    }
    get age(){
        return this.age;
    }
    set age(value) {
        console.log(`Передано ${value}`);
        this.age = value;
    }
}
class Programmer extends Employee {
    constructor(name, age, salary, lang){
        super(name, age, salary)
        this.lang = lang
    }
    get newSalary(){
        // console.log(`salary: (${this.salary} * 3)`);
        return this.salary * 3;
        // this._salary = value * 3;
    }
}

let person = new Employee('Jonh', "37", "20000",'English');
let person1 = new Programmer('David', '30', '23000', 'French')
let person2 = new Programmer('Mike', '20', '18000', 'Spanish')
console.log(person1);
console.log(person2);
console.log(person1.newSalary)
console.log(person2.newSalary)
